import models.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Class for {@link DepositService}
 */
class DepositServiceTest {

    /**
     * Tests for method {@link DepositService#checkCompanyBalance(Company, Float)}.
     */
    @org.junit.jupiter.api.Test
    void checkCompanyBalance() {
        DepositService depositService = new DepositService();
        Company company = new Company();
        company.setBalance(1000f);

        assertFalse(depositService.checkCompanyBalance(null, null));
        assertFalse(depositService.checkCompanyBalance(company, null));
        assertFalse(depositService.checkCompanyBalance(null, 1000f));
        assertFalse(depositService.checkCompanyBalance(company, 1001f));

        assertTrue(depositService.checkCompanyBalance(company, 1f));
        assertTrue(depositService.checkCompanyBalance(company, 1000f));
    }

    /**
     * Tests for method {@link DepositService#verifyUserBalance(User, DepositType)}.
     */
    @org.junit.jupiter.api.Test
    void verifyUserBalance() {
        DepositService depositService = new DepositService();
        User user = new User();

        assertEquals(0, depositService.verifyUserBalance(user, DepositType.UNKNOWN));
        assertEquals(0, depositService.verifyUserBalance(user, DepositType.GIFT));
        assertEquals(0, depositService.verifyUserBalance(user, DepositType.MEAL));

        user.setGiftsList(createFakeListOfGifts());
        user.setMealsList(createFakeListOfMeals());

        assertEquals(130, depositService.verifyUserBalance(user, DepositType.GIFT));
        assertEquals(130, depositService.verifyUserBalance(user, DepositType.MEAL));
    }

    /**
     * Create of Fake list of Gift Deposits.
     */
    List<GiftDeposit> createFakeListOfGifts() {
        List<GiftDeposit> giftsList = new ArrayList<>();

        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());
        c1.add(Calendar.DATE, -365);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        c2.add(Calendar.DATE, -396);

        GiftDeposit d1 = new GiftDeposit(1, 50, 0, new Date(), new Company());
        GiftDeposit d2 = new GiftDeposit(1, 50, 10, new Date(), new Company());
        GiftDeposit d3 = new GiftDeposit(1, 50, 10, c1.getTime(), new Company()); // 1 Year ago Still OK
        GiftDeposit d4 = new GiftDeposit(1, 50, 10, c2.getTime(), new Company()); // 1 Year ago + 1 Day KO

        giftsList.add(d1);
        giftsList.add(d2);
        giftsList.add(d3);
        giftsList.add(d4);
        return giftsList;
    }

    /**
     * Create of Fake list of Meal Deposits.
     */
    List<MealDeposit> createFakeListOfMeals() {
        List<MealDeposit> mealsList = new ArrayList<>();

        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());
        c1.add(Calendar.DATE, -365);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        c2.add(Calendar.DATE, -400);

        MealDeposit d1 = new MealDeposit(1, 50, 0, new Date(), new Company());
        MealDeposit d2 = new MealDeposit(1, 50, 10, new Date(), new Company());
        MealDeposit d3 = new MealDeposit(1, 50, 10, c1.getTime(), new Company()); // 1 Year ago Still OK
        MealDeposit d4 = new MealDeposit(1, 50, 10, c2.getTime(), new Company()); // 1 Year ago + 35 Day KO

        mealsList.add(d1);
        mealsList.add(d2);
        mealsList.add(d3);
        mealsList.add(d4);
        return mealsList;
    }
}
