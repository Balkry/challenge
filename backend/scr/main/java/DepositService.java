import models.Company;
import models.Deposit;
import models.DepositType;
import models.User;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

public class DepositService {

    /**
     * Check if the company can distribute a gift.
     *
     * @param company            Company to check
     * @param amountToDistribute
     * @return true if distribution is available
     */
    public boolean checkCompanyBalance(final Company company, final Float amountToDistribute) {
        if (company != null && amountToDistribute != null && company.getBalance() >= amountToDistribute) {
            System.out.println("Balance allow to distribute gift");
            return true;
        }
        System.out.println("Balance cannot allow to distribute gift");
        return false;
    }

    /**
     * Verify balance of the User for GIFT or MEAL.
     *
     * @param user        to test
     * @param depositType Type of Deposit
     * @return Value of the balance.
     */
    public float verifyUserBalance(final User user, final DepositType depositType) {
        switch (depositType) {
            case GIFT:
                return getGiftUserBalance(user.getGiftsList(), DepositType.GIFT);
            case MEAL:
                return getGiftUserBalance(user.getMealsList(), DepositType.MEAL);
            default:
                return 0;
        }
    }

    /**
     * Iterate on all User's Deposits to check available amount.
     *
     * @param deposits    List of all the User's Deposits
     * @param depositType Type of Deposit
     * @return Value of the balance.
     */
    private float getGiftUserBalance(final List<?> deposits, final DepositType depositType) {
        float balance = 0f;
        for (Deposit deposit : (List<Deposit>) deposits) {
            balance = balance + getAmountAvailable(deposit, depositType);
        }
        return balance;
    }

    /**
     * For a Deposit, check if it has not expired the and if  there's always money.
     *
     * @param deposit to check
     * @param type    GIFT or MEAL
     * @return amount available for a deposit
     */
    private float getAmountAvailable(final Deposit deposit, final DepositType type) {
        float amountAvailable = 0f;

        // Case GIFT
        LocalDate now = LocalDate.now();

        LocalDate initialDate = deposit.getReceivedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate initialDateExpiration = null;

        if (type == DepositType.GIFT) {
            initialDateExpiration = initialDate.plusDays(365);
        }
        if (type == DepositType.MEAL) {
            initialDateExpiration = initialDate.plusDays(365);
            initialDateExpiration = initialDateExpiration.with(TemporalAdjusters.lastDayOfMonth());
        }

        if (initialDateExpiration != null) {
            if (now.isAfter(initialDateExpiration)) {
                amountAvailable = 0f;
            } else {
                amountAvailable = deposit.getInitialAmount() - deposit.getUsedAmount();
            }
        }
        return amountAvailable;
    }
}

