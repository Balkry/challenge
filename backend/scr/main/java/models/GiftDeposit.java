package models;

import java.util.Date;

public final class GiftDeposit extends Deposit {
    public GiftDeposit() {
        super();
    }

    public GiftDeposit(int id, float initialAmount, float usedAmount, Date receivedDate, Company distributor) {
        super(id, initialAmount, usedAmount, receivedDate, distributor);
    }
}