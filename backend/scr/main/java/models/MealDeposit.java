package models;

import java.util.Date;

public final class MealDeposit extends Deposit {
    public MealDeposit() {
        super();
    }

    public MealDeposit(int id, float initialAmount, float usedAmount, Date receivedDate, Company distributor) {
        super(id, initialAmount, usedAmount, receivedDate, distributor);
    }
}
