package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {

    int id;
    String userName;
    List<GiftDeposit> giftsList = new ArrayList<>();
    List<MealDeposit> mealsList = new ArrayList<>();

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<GiftDeposit> getGiftsList() {
        return giftsList;
    }

    public void setGiftsList(List<GiftDeposit> giftsList) {
        this.giftsList = giftsList;
    }

    public List<MealDeposit> getMealsList() {
        return mealsList;
    }

    public void setMealsList(List<MealDeposit> mealsList) {
        this.mealsList = mealsList;
    }
}