package models;

import java.util.Date;

public class Deposit {

    int id;
    float initialAmount;
    float usedAmount;
    java.util.Date receivedDate;
    Company distributor;

    public Deposit() {
    }

    public Deposit(int id, float initialAmount, float usedAmount, Date receivedDate, Company distributor) {
        this.id = id;
        this.initialAmount = initialAmount;
        this.usedAmount = usedAmount;
        this.receivedDate = receivedDate;
        this.distributor = distributor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(float initialAmount) {
        this.initialAmount = initialAmount;
    }

    public float getUsedAmount() {
        return usedAmount;
    }

    public void setUsedAmount(float usedAmount) {
        this.usedAmount = usedAmount;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Company getDistributor() {
        return distributor;
    }

    public void setDistributor(Company distributor) {
        this.distributor = distributor;
    }
}
